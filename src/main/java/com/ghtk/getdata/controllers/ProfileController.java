package com.ghtk.getdata.controllers;

import com.ghtk.getdata.dtos.UserProfileDto;
import com.ghtk.getdata.entities.ProfileEntity;
import com.ghtk.getdata.services.ProfileService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1.0/profile")
@RequiredArgsConstructor
public class ProfileController {

  private final ProfileService profileService;

  // Lấy danh sách profiles theo username
  @GetMapping("/{username}")
  public ResponseEntity<List<ProfileEntity>> getProfilesByUser(
      @PathVariable("username") String username) {
    return ResponseEntity.ok(profileService.getProfilesByUser(username));
  }

  // Lấy ra profile mới nhất của user
  @GetMapping("/latest/{username}")
  public ResponseEntity<ProfileEntity> getLatestProfileByUser(
      @PathVariable("username") String username) {
    return ResponseEntity.ok(profileService.getLatestProfileByUser(username));
  }

  // Lấy ra thông tin profiles của các user
  @GetMapping("/all")
  public ResponseEntity<List<UserProfileDto>> getUserProfiles() {
    return ResponseEntity.ok(profileService.getUserProfiles());
  }

  // Tìm kiếm profiles
  @GetMapping("/search")
  public ResponseEntity<List<UserProfileDto>> searchUserProfiles(
      @RequestParam(name = "username", required = false, defaultValue = "") String username,
      @RequestParam(name = "code", required = false, defaultValue = "") String profileCode) {
    return ResponseEntity.ok(profileService.searchUserProfiles(username, profileCode));
  }

  // Tìm kiếm profiles V2
  @GetMapping("/search/v2")
  public ResponseEntity<List<ProfileEntity>> searchUserProfilesV2(
      @RequestParam(name = "username", required = false, defaultValue = "") String username,
      @RequestParam(name = "code", required = false, defaultValue = "") String profileCode,
      @RequestParam(name = "name", required = false, defaultValue = "") String profileName) {
    return ResponseEntity.ok(profileService.searchUserProfilesV2(profileCode, profileName));
  }
}
