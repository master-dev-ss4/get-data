package com.ghtk.getdata.controllers;

import com.ghtk.getdata.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1.0/user")
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;

  @GetMapping("/{userId}")
  public ResponseEntity<Object> getUser(@PathVariable("userId") Integer userId) {
    return ResponseEntity.ok(userService.getUserById(userId));
  }

  @GetMapping("/username/{username}")
  public ResponseEntity<Object> getUserByUsername(@PathVariable("username") String username) {
    return ResponseEntity.ok(userService.getUserByUsername(username));
  }
}
