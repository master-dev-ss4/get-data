package com.ghtk.getdata.dtos;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SqlResultSetMapping(
    name = "UserProfileDto",
    classes =
        @ConstructorResult(
            targetClass = UserProfileDto.class,
            columns = {
              @ColumnResult(name = "username", type = String.class),
              @ColumnResult(name = "name", type = String.class),
              @ColumnResult(name = "code", type = String.class),
            }))
@Entity
@Getter
@Setter
@NoArgsConstructor
public class UserProfileDto {

  @Id private String username;

  private String name;

  private String code;

  public UserProfileDto(String username, String name, String code) {
    this.username = username;
    this.name = name;
    this.code = code;
  }
}
