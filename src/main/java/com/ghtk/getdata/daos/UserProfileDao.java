package com.ghtk.getdata.daos;

import com.ghtk.getdata.dtos.UserProfileDto;
import com.ghtk.getdata.entities.ProfileEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
@RequiredArgsConstructor
public class UserProfileDao {

  private final EntityManager entityManager;

  public List<UserProfileDto> getUserProfiles() {
    String strQuery =
        " "
            + " SELECT u.username AS username, "
            + "        p.name AS name, "
            + "        p.code AS code "
            + " FROM users u "
            + "   JOIN profiles p ON p.user_id = u.id ";
    Query query = entityManager.createNativeQuery(strQuery, "UserProfileDto");
    return query.getResultList();
  }

  public List<UserProfileDto> searchUserProfiles(String username, String profileCode) {
    StringBuilder builder =
        new StringBuilder(
            " "
                + " SELECT u.username AS username, "
                + "        p.name AS name, "
                + "        p.code AS code "
                + " FROM users u "
                + "   JOIN profiles p ON p.user_id = u.id "
                + " WHERE 1 = 1 ");
    if (StringUtils.hasText(username)) {
      builder.append(" AND u.username = :username ");
    }
    if (StringUtils.hasText(profileCode)) {
      builder.append(" AND p.`code` = :profileCode ");
    }
    Query query = entityManager.createNativeQuery(builder.toString(), "UserProfileDto");
    if (StringUtils.hasText(username)) {
      query.setParameter("username", username);
    }
    if (StringUtils.hasText(profileCode)) {
      query.setParameter("profileCode", profileCode);
    }
    return query.getResultList();
  }

  public List<ProfileEntity> searchUserProfilesV2(String profileCode, String profileName) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<ProfileEntity> cq = cb.createQuery(ProfileEntity.class);

    Root<ProfileEntity> profile = cq.from(ProfileEntity.class);
    List<Predicate> predicates = new ArrayList<>();

    if (StringUtils.hasText(profileCode)) {
      predicates.add(cb.equal(profile.get("code"), profileCode));
    }

    if (StringUtils.hasText(profileName)) {
      predicates.add(cb.equal(profile.get("name"), profileName));
    }

    cq.where(predicates.toArray(new Predicate[0]));

    TypedQuery<ProfileEntity> query = entityManager.createQuery(cq);
    return query.getResultList();
  }
}
