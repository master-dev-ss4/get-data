package com.ghtk.getdata.services;

import com.ghtk.getdata.dtos.UserProfileDto;
import com.ghtk.getdata.entities.ProfileEntity;
import java.util.List;

public interface ProfileService {

  List<ProfileEntity> getProfilesByUser(String username);

  ProfileEntity getLatestProfileByUser(String username);

  List<UserProfileDto> getUserProfiles();

  List<UserProfileDto> searchUserProfiles(String username, String profileCode);

  List<ProfileEntity> searchUserProfilesV2(String profileCode, String profileName);
}
