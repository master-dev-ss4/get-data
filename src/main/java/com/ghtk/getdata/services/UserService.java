package com.ghtk.getdata.services;

import com.ghtk.getdata.entities.UserEntity;

public interface UserService {

  UserEntity getUserById(Integer id);

  UserEntity getUserByUsername(String username);
}
