package com.ghtk.getdata.services.impl;

import com.ghtk.getdata.entities.UserEntity;
import com.ghtk.getdata.repositories.UserRepository;
import com.ghtk.getdata.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Override
  public UserEntity getUserById(Integer id) {
    return userRepository.findById(id).orElse(null);
  }

  @Override
  public UserEntity getUserByUsername(String username) {
    return userRepository.findFirstByUsername(username);
  }
}
