package com.ghtk.getdata.services.impl;

import com.ghtk.getdata.daos.UserProfileDao;
import com.ghtk.getdata.dtos.UserProfileDto;
import com.ghtk.getdata.entities.ProfileEntity;
import com.ghtk.getdata.repositories.ProfileRepository;
import com.ghtk.getdata.services.ProfileService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {

  private final ProfileRepository profileRepository;
  private final UserProfileDao userProfileDao;

  @Override
  public List<ProfileEntity> getProfilesByUser(String username) {
    return profileRepository.getProfilesByUser(username);
  }

  @Override
  public ProfileEntity getLatestProfileByUser(String username) {
    return profileRepository.getLatestProfileByUser(username);
  }

  @Override
  public List<UserProfileDto> getUserProfiles() {
    return userProfileDao.getUserProfiles();
  }

  @Override
  public List<UserProfileDto> searchUserProfiles(String username, String profileCode) {
    return userProfileDao.searchUserProfiles(username, profileCode);
  }

  @Override
  public List<ProfileEntity> searchUserProfilesV2(String profileCode, String profileName) {
    return userProfileDao.searchUserProfilesV2(profileCode, profileName);
  }
}
