package com.ghtk.getdata.repositories;

import com.ghtk.getdata.entities.ProfileEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends CrudRepository<ProfileEntity, Integer> {

  @Query(
      value =
          " "
              + " SELECT p "
              + " FROM ProfileEntity p "
              + "   JOIN UserEntity u ON u.id = p.userId "
              + " WHERE u.username = :username")
  List<ProfileEntity> getProfilesByUser(String username);

  @Query(
      value =
          " "
              + " SELECT p.* "
              + " FROM profiles p "
              + "   JOIN users u ON u.id = p.user_id "
              + " WHERE u.username = :username "
              + " ORDER BY p.id DESC LIMIT 1 ",
      nativeQuery = true)
  ProfileEntity getLatestProfileByUser(String username);
}
