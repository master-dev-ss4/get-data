package com.ghtk.getdata.repositories;

import com.ghtk.getdata.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

  UserEntity findFirstByUsername(String username);
}
