-- Tạo bảng
CREATE TABLE profiles
(
    id      INTEGER      NOT NULL AUTO_INCREMENT,
    name    VARCHAR(255) NOT NULL,
    code    VARCHAR(255) NOT NULL,
    user_id INTEGER      NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE users
(
    id        INTEGER      NOT NULL AUTO_INCREMENT,
    full_name VARCHAR(255) NOT NULL,
    username  VARCHAR(255) NOT NULL,
    email     VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

-- Tạo dữ liệu mẫu
INSERT INTO users (id, username, full_name, email)
VALUES (1, 'namt', 'Trần Nam', 'namt@ghtk.co');

INSERT INTO users (id, username, full_name, email)
VALUES (2, 'binhnn', 'Nguyễn Nhật Bình', 'binhnn@ghtk.co');

INSERT INTO profiles (id, name, code, user_id)
VALUES (1, 'Profile 1', 'PROFILE_NAMT_1', 1);
INSERT INTO profiles (id, name, code, user_id)
VALUES (2, 'Profile 2', 'PROFILE_NAMT_2', 1);
INSERT INTO profiles (id, name, code, user_id)
VALUES (3, 'Profile 1', 'PROFILE_BINHNN_1', 2);
INSERT INTO profiles (id, name, code, user_id)
VALUES (4, 'Profile 2', 'PROFILE_BINHNN_2', 2);